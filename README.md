‼️ loimpute in this form is unmaintained and the state-of-the-art imputation workflow for low-pass sequencing is available as a SaaS offering on [https://gencove.com](https://gencove.com)


# LOIMPUTE

LOIMPUTE is a tool designed for genotype imputation from low-pass sequencing data (generally under 1x coverage of a genome). The model was initially described in [Wasik et al. (2019)](https://www.biorxiv.org/content/10.1101/632141v1) "Comparing low-pass sequencing and genotyping for trait mapping in pharmacogenetics".

For additional applications see:

- [Li et al. (2020)](https://www.biorxiv.org/content/10.1101/2020.04.29.068452v1) "Low-pass sequencing increases the power of GWAS and decreases measurement error of polygenic risk scores compared to genotyping arrays"

- [Martin et al. (2020)](https://www.biorxiv.org/content/10.1101/2020.04.27.064832v1) "Low-coverage sequencing cost-effectively detects known and novel variation in underrepresented populations"

- [Fuller et al. (2019)](https://www.biorxiv.org/content/10.1101/867754v1) "Population genetics of the coral Acropora millepora: Towards a genomic predictor of bleaching"

# Access

We have made LOIMPUTE available for academic use, please request access [here](https://gencove.typeform.com/to/AWqamE), we will provide access to a Docker image.

# License

Available [here](https://docs.google.com/document/d/1wFHTROz_vuE9DkxGQmTKRNHyqYo3SR94dCOOb9f487U/edit?usp=sharing)

# Quick Start

```
git clone git@gitlab.com:gencove/loimpute-public.git # get this readme and the example data
docker pull gencove/loimpute:latest # get the latest loimpute build

docker run -v $PWD/loimpute-public/test/:/data/ -it gencove/loimpute:latest \\
 -i /data/test_input.mp.gz \\
 -h /data/test_refpanel.vcf.gz \\
 -o /data/testout
```

# Install binary

If you prefer to use the static binary (compatible with Linux 64-bit Intel processors), it can be locally installed:

```
id=$(docker create gencove/loimpute:latest)
docker cp $id:/usr/local/bin/loimpute .  
docker rm -v $id
```

# Usage

# Required flags

```
-i gzipped input file in mpileup format
-h gzipped reference panel in phased VCF format
-o output stem
```

# Input file format

The input file for imputation is gzipped output from [samtools](http://www.htslib.org/) mpileup command (run without a reference genome). 

This is just a list of chromosomes and positions along with the bases that cover the positions, like:

```
1       1330931 N       1       A 
1       1596979 N       1       C       
1       1779036 N       1       C       
1       2338569 N       1       c       
1       2339115 N       1       T       
1       2792801 N       1       c       
1       2896784 N       1       G       
1       3044181 N       1       c    
```

An example is available in `test/test_input.mp.gz`.

To generate this from a BAM file the command is `samtools mpileup -l sitelist input.bam | gzip -c > loimpute_input.gz`, where the input BAM file is `input.bam`, the output (in turn the input to loimpute) is `loimpute_input.gz`, and the list of sites to impute is `sitelist` (an example is in `test/example_sitelist.gz`)

# Reference panel format

Phased [VCF](https://samtools.github.io/hts-specs/VCFv4.2.pdf). We have used the [phase 3 data](ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/) from the 1000 Genomes Project or the Haplotype Reference Consortium.

An example is in `test/test_refpanel.vcf.gz`.

We recommend running imputation in approximately 2Mb chunks with a small buffer on either side. Pre-split chunks for the 1000 Genomes Phase 3 dataset (correcting some duplicate sites present in the official release) are available on AWS S3 at `s3://gencove-sbir/imputation-reference/`

# Output file format

Unphased, bgzipped [VCF](https://samtools.github.io/hts-specs/VCFv4.2.pdf). The GT field contains the best guess genotype, the GP field contains genotype probabilities, and the DS field contains alternate allele dosages. The FILTER field is set to PASS or LOWCONF depending on genotype probability thresholds defined below.

# Parameters

`-k [int]`: controls the number of reference haplotypes on which to run the Li and Stephens algorithm. Run time increases proportional to k^2 and accuracy increases somewhat with higher k values. The default value is 80.

`-id [string]`: the ID of the individual to be passed to the output VCF. Defaults to the same as `-o`.

`-e [float]`: the assumed sequencing error rate. Defaults to 0.001.

`-ne [int]`: the assumed effective population size of the populaion from which the individual is drawn. Defaults to 10,000.

`-m [int]`: the maximum number of reads to use at a site in the genome. Defaults to 5.

`-min [int]`: the minimum number of sites in the reference panel VCF that must be covered by at least one sequencing read. Defaults to 1000.

`-range [int] [int]`: the start and stop positions of the region to print to the output file. Defaults to output every site in the reference panel VCF.

`-reference_name [string]`: ame of reference genome to print in header of the output VCF

`-imputation_panel [string]`: name of imputation reference panel to print in header of the output VCF

`-gpf [float]`: the probability threshold below which the LOWCONF filter is set for a genotype. I.e. if no genotype has a probability above this threshold the site will be flagged as LOWCONF. Default is 0.9.

`-skip [string]`: sample id of an individual in the reference haplotype file that should be left out during imputation. By default all samples are used.



